# Nebula

## Setup

Nebula depends the following:

- PHP 7.4
- Composer
- Laravel 8
  - Mix 6
  - Sails
- MySQL 8
- Node 16
- Vue 3

Run the following commands to setup the project:

```bash
$ composer install
$ npm install
```

## Development

As a prerequisite, generate a `.env` file. You can use the example file as reference.

```bash
$ cp .env.example .env
```

and then, fill out the required fields. After that, generate your 
application key and use it to fill out the APP_KEY field

```bash
$ php artisan key:generate
```

It is recommended to develop with [Laravel Sail](https://laravel.com/docs/8.x/sail), which in turn require `docker`
and `docker-compose` to be installed on your machine.

After installing `docker` and `docker-compose` on your machine, start development by running

```bash
$ ./vendor/bin/sail up
```
