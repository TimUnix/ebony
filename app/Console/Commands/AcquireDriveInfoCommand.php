<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

use App\Models\Drive;
use App\Utils\Drive\InfoScanner;

class AcquireDriveInfoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drives:acquire_info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $activeDrives = Drive::whereNot('device_unix_status', 'inactive')
                      ->get()
                      ->each(function($drive) {
                          $process = Process::fromShellCommandline("sudo smartctl -T permissive -x -j "
                                                                   . "/dev/{$drive->device_unix_name}");
                          $process->run();
                          $smartInfo = json_decode($process->getOutput());

                          $drive->update([
                              'device_blocks' => $smartInfo->logical_block_size ?? null,
                              'device_defects' => $smartInfo->scsi_grown_defect_list ?? null,
                              'device_power_hrs' => $smartInfo->power_on_time?->hours ?? null,
                          ]);
                      });

        return Command::SUCCESS;
    }
}
