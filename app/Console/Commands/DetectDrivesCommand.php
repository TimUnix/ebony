<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Utils\Drive\Detector;

class DetectDrivesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drives:detect';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Detect active drives';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {
        $drives = Detector::run();
        $message = "$drives new drives detected";
        \Log::info($message);
        $this->info($message);

        return Command::SUCCESS;
    }
}
