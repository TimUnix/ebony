<?php

namespace App\Http\Livewire;

use App\Models\Drive;
use Livewire\Component;

class ConfirmationModal extends Component {
    public ?string $message;
    public ?Drive $drive;
    public int $conversionType;
    public int $blockSize;

    protected $listeners = [
        'multipleConversionPrompted' => 'promptMultiple',
        'singleConversionPrompted' => 'promptSingle'
    ];

    public function mount() {
        $this->conversionType = -1;
    }

    public function render() {
        return view('livewire.confirmation-modal');
    }

    public function promptMultiple(int $blockSize) {
        $this->message = "You are about to convert multiple drives to $blockSize.";
        $this->blockSize = $blockSize;
        $this->conversionType = 0;
    }

    public function promptSingle(?Drive $drive) {
        $this->message = "You are about to convert {$drive->device_serial_number}.";
        $this->drive = $drive;
        $this->conversionType = 1;
    }

    public function confirmConversion() {
        if ($this->conversionType === 0) {
            $this->emitUp('multipleConversionConfirmed', $this->blockSize);
        } else if ($this->conversionType === 1) {
            $this->emitUp('singleConversionConfirmed', $this->drive);
        }
    }
}
