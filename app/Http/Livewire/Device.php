<?php

namespace App\Http\Livewire;

use App\Models\ActiveDrives;
use App\Models\Drive;
use App\Models\ConversionLog;
use Livewire\Component;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessTimedOutException;

class Device extends Component {
    public ?Drive $drive;

    protected $listeners = [
        'drivesUpdated' => '$refresh'
    ];

    public function mount(?Drive $drive) {
        $this->drive = $drive;
    }

    public function render() {
        return view('livewire.device');
    }

    public function handleConvertClicked() {
        $this->emitUp('singleConversionClicked', $this->drive);
    }

    public function handleLocateClicked() {
        $this->emitUp('locateClicked', $this->drive);
    }

    public function refreshProgress(?string $device, ?string $serialNumber) {
        if (env('APP_ENV') === 'production') {
            $progressProcess = Process::fromShellCommandline("sudo sg_requests --progress /dev/$device");

            try {
                $progressProcess->run();
                $progressOutput = $progressProcess->getOutput();
            } catch (ProcessTimedOutException $exception) {
                \Log::debug("Process timed out while trying to poll progress for $serialNumber at $device");
            }

            $progress = filter_var($progressOutput, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

            // When the output returns an emtpy string and the drive was last known to be in conversion, assume
            // conversion is done as the sg_requests command returns a blank string when run and conversion
            // is done.
            if (empty($progress) && str($this->drive->device_unix_status)->contains('converting') &&
                $this->drive->device_conversion_progress >= 0) {
                $updatedBlocks = $this->drive->device_blocks === 512 ? 520 : 512;

                $this->drive->update([
                    'device_unix_status' => 'ready',
                    'device_conversion_progress' => 100,
                    'device_blocks' => $updatedBlocks
                ]);
            } else {
                $this->drive->update([ 'device_conversion_progress' => empty($progress) ? 0 : $progress ]);
            }
        }
    }
}
