<?php

namespace App\Http\Livewire\Pages\Checkin;

use Livewire\Component;

use Carbon\Carbon;

use App\Models\Drive;

class Checkin extends Component {
    public ?string $poNumber = null;
    public ?string $partNumber = null;
    public ?int $capacity = null;
    public ?string $serialNumber = null;
    public int $measurement = 0;

    protected $rules = [
        'poNumber' => 'required',
        'partNumber' => 'required',
        'capacity' => 'required|integer',
        'serialNumber' => 'required',
    ];

    public function render() {
        return view('livewire.pages.checkin.index');
    }

    public function save() {
        $this->validate();

        $unconvertedCapacity = $this->capacity;
        $capacity = $this->measurement === 1 ? $unconvertedCapacity * 1000 : $unconvertedCapacity;

        Drive::insert([
            'device_po_number' => $this->poNumber,
            'device_part_number' => $this->partNumber,
            'device_capacity' => $capacity,
            'device_serial_number' => $this->serialNumber,
            'created_at' => Carbon::now()
        ]);

        $this->serialNumber = null;
    }
}
