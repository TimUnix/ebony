<?php

namespace App\Http\Livewire\Pages;

use App\Models\Drive;
use App\Utils\Drive\Converter;
use App\Utils\Drive\Detector;
use App\Utils\Drive\Locator;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Collection;

class Home extends Component {

    public ?Collection $drives;
    public bool $modalShown;

    protected $listeners = [
        'conversionCancelled' => 'hideConfirmationModal',
        'singleConversionClicked' => 'promptSingleConversion',
        'singleConversionConfirmed' => 'convertDrive',
        'multipleConversionConfirmed' => 'convertDrives',
        'locateClicked' => 'locateDrive'
    ];

    public function mount() {
        Detector::run();

        $this->drives = Drive::loadDrives();
        $this->modalShown = false;
    }

    public function refreshDrives() {
        $this->drives = Drive::loadDrives();
        $this->emit('drivesUpdated');
    }

    public function render() {
        return view('livewire.pages.home');
    }

    public function getStatusProperty() {
        return [
            '512' => $this->drives->filter(fn($drive) => $drive->device_blocks === 512)->count(),
            'other' => $this->drives->filter(fn($drive) => $drive->device_blocks !== 512)->count(),
            'convertingTo512' => $this->drives->filter(fn($drive) => str($drive->device_unix_status)->contains('512'))
                ->count(),
            'convertingTo520' => $this->drives->filter(fn($drive) => str($drive->device_unix_status)->contains('520'))
                ->count()
        ];
    }

    public function getSummaryProperty() {
        return $this->drives->reduce(function($summary, $drive) {
            array_key_exists("{$drive->device_capacity}", $summary)
                ? $summary["{$drive->device_capacity}"]++
                : $summary["{$drive->device_capacity}"] = 1;

            return $summary;
        }, []);
    }

    public function getDriveCountProperty() {
        return $this->drives->count();
    }

    public function locateDrive(?Drive $drive) {
        if (env('APP_ENV') === 'production') {
            Locator::run($drive);
        }
    }

    public function convertDrives(int $blockSize) {
        if (env('APP_ENV') === 'production') {
            if ($blockSize === 512) {
                $this->drives->each(function($drive) {
                    if ($drive->device_unix_status === 'ready' && $drive->blocks !== 512) {
                        Converter::run512($drive->device_unix_name);
                    }
                });
            } else {
                $this->drives->each(function($drive) {
                    if ($drive->device_unix_status === 'ready' && $drive->blocks === 512) {
                        Converter::run520($drive->device_unix_name);
                    }
                });
            }
        }

        if ($blockSize === 512) {
            Drive::where('device_unix_status', 'ready')
                ->whereNot('device_blocks', 512)
                ->update([
                    'device_unix_status' => 'converting to 512',
                    'device_conversion_progress' => 0
                ]);
        } else {
            Drive::where('device_unix_status', 'ready')
                ->where('device_blocks', 512)
                ->update([
                    'device_unix_status' => 'converting to 520',
                    'device_conversion_progress' => 0
                ]);
        }

        $this->drives = Drive::loadDrives();
        $this->hideConfirmationModal();
        $this->emit('drivesUpdated');
    }

    public function convertDrive(?Drive $drive) {
        if (env('APP_ENV') === 'production') {
            if ($drive->device_blocks === 512) {
                Converter::run520($drive->device_unix_name);
            } else {
                Converter::run512($drive->device_unix_name);
            }
        }

        if ($drive->device_blocks === 512) {
            $drive->device_unix_status = 'converting to 520';
        } else {
            $drive->device_unix_status = 'converting to 512';
        }

        $drive->device_conversion_progress = 0;
        $drive->save();

        $this->hideConfirmationModal();
        $this->emit('drivesUpdated');
    }

    public function promptSingleConversion(?Drive $drive) {
        $this->modalShown = true;
        $this->emitTo('confirmation-modal', 'singleConversionPrompted', $drive);
    }

    public function promptMultipleConversion(int $blockSize) {
        $this->modalShown = true;
        $this->emitTo('confirmation-modal', 'multipleConversionPrompted', $blockSize);
    }

    public function hideConfirmationModal() {
        $this->modalShown = false;
    }
}
