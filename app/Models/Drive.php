<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Drive extends Model {
    use HasFactory;

    protected $guarded = [];

    public static function loadDrives(): Collection {
        return Drive::whereNot('device_unix_status', 'inactive')->get();
    }

    public static function deactivateUndetected(Collection $serials): int {
        return Drive::whereNotIn('device_serial_number', $serials)->update(['device_unix_status' => 'inactive']);
    }
}
