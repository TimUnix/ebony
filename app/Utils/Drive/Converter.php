<?php

namespace App\Utils\Drive;

use Symfony\Component\Process\Process;

class Converter {
    public static function run512(string $drive) {
        $process = Process::fromShellCommandline("sudo sg_format --format --size=512 --fmtpinfo=0 "
                                                 . "/dev/{$drive}");
        $process->start();
    }

    public static function run520(string $drive) {
        $process = Process::fromShellCommandline("sudo sg_format --format --size=520 --fmtpinfo=0 "
                                                 . "/dev/{$drive}");
        $process->start();
    }
}
