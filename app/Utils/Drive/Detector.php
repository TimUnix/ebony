<?php

namespace App\Utils\Drive;

use Illuminate\Support\Collection;
use Symfony\Component\Process\Process;

use App\Models\Drive;
use App\Utils\Drive\InfoScanner;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;

class Detector {
    public static function run(): int {
        $sentinel = Storage::disk('disk_tests')->path('hdsentinel-019c-x64');
        $reportPath = Storage::disk('local')->path('sentinel.xml');

        $process = new Process([ 'sudo', $sentinel, '-xml', '-r', $reportPath ]);
        $process->run();

        $report = Storage::disk('local')->get('sentinel.xml');
        $report = simplexml_load_string($report);
        $info = $report->children();

        $detectedDrives = [];
        $activeSerials = [];

        foreach ($info as $drive) {
            if (in_array($drive->getName(), ['General_Information', 'Partition_Information'])) continue;
            if ((string) $drive->{'Hard_Disk_Summary'}->{'Hard_Disk_Device'} === '/dev/sda') continue;

            $serialNumber = (string) $drive->{'Hard_Disk_Summary'}->{'Hard_Disk_Serial_Number'};
            $modelFamily = (string) $drive->{'Hard_Disk_Summary'}->{'Hard_Disk_Model_ID'};
            $device = str_replace('/dev/', '', (string) $drive->{'Hard_Disk_Summary'}->{'Hard_Disk_Device'});
            // hdsentinel shows capacity in MB so dividing by 1000 gets the GB value
            $capacity = round(
                ((int) preg_replace("/[^0-9]/", '', (string) $drive->{'Hard_Disk_Summary'}->{'Total_Size'})) / 1000
            );
            // hdsentinel displays power on time in days, hours [, minutes ] format
            $powerHrs = (string) $drive->{'Hard_Disk_Summary'}->{'Power_on_time'};

            if (!empty($powerHrs)) {
                $powerHrs = explode(',', $powerHrs);
                $powerDays = (int) preg_replace("/[^0-9]/", '', (string) $powerHrs[0]);
                $powerHrs = (int) preg_replace("/[^0-9]/", '', (string) $powerHrs[1]);
                $powerHrs = $powerDays * 24 + $powerHrs;
            }

            $health = preg_replace("/[^0-9]/", '', (string) $drive->{'Hard_Disk_Summary'}->{'Health'});

            if (empty($health)) {
                $health = null;
            } else {
                $health = (int) $health;
            }

            if (!empty($drive->{'Properties'})) {
                $blocks = (int) preg_replace("/[^0-9]/", '',
                                             (string) $drive->{'Properties'}->{'Bytes_Per_Sector'});
            }

            if (!empty($drive->{'ATA_Information'})) {
                $blocks = (int) preg_replace("/[^0-9]/", '',
                                             (string) $drive->{'ATA_Information'}->{'Bytes_Per_Sector'});
            }

            $activeSerials[] = $serialNumber;
            $detectedDrives[] = [
                'device_serial_number' => $serialNumber,
                'device_model_family' => $modelFamily,
                'device_unix_name' => $device,
                'device_capacity' => $capacity,
                'device_power_hrs' => empty($powerHrs) ? 0 : $powerHrs,
                'device_health' => $health,
                'device_unix_status' => 'ready',
                'device_blocks' => $blocks ?? null
            ];
        }

        $existingSerials = Drive::select('device_serial_number')
            ->whereIn('device_serial_number', $activeSerials)
            ->get()
            ->map(fn($drive) => $drive->device_serial_number)
            ->toArray();

        $newDrives = collect($detectedDrives)
            ->filter(fn($drive) => !in_array($drive['device_serial_number'], $existingSerials))
            ->toArray();

        Drive::deactivateUndetected(collect($activeSerials));

        if (empty($newDrives)) {
            return 0;
        }

        if (!Drive::insert($newDrives)) {
            return 0;
        }

        return count($newDrives);
    }
}
