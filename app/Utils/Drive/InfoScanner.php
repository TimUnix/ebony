<?php

namespace App\Utils\Drive;

use Illuminate\Support\Collection;
use Symfony\Component\Process\Process;

class InfoScanner {
    public static function runShort(array $drive): array {
        $driveName = $drive['device'];
        $logicalId = $drive['logical_id'];

        $infoProcess = Process::fromShellCommandline("sudo smartctl -T permissive -i -j /dev/$driveName");
        $infoProcess->run();
        $smartOutput = $infoProcess->getOutput();
        $smartInfo = json_decode($smartOutput);

        $driveInfo = [
            'device_unix_name' => $driveName,
            'device_logical_id' => $logicalId,
            'device_serial_number' => $smartInfo->serial_number ?? '',
            'device_part_number' =>  $smartInfo->product ?? '',
            'device_model_family' => $smartInfo->model_name ?? '',
            'device_blocks' => $smartInfo->logical_block_size ?? null,
            'device_defects' => $smartInfo->scsi_grown_defect_list ?? 0,
            'device_power_hrs' => $smartInfo->power_on_time?->hours ?? 0,
            'device_capacity' => 0,
            'device_unix_status' => 'ready',
        ];

        if (!empty($smartInfo->user_capacity?->bytes)) {
            $driveInfo['device_capacity'] = $smartInfo->user_capacity?->bytes / 1000000000;
        }

        return $driveInfo;
    }
}
