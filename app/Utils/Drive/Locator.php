<?php

namespace App\Utils\Drive;

use Symfony\Component\Process\Process;
use App\Models\Drive;

class Locator {
    public static function run(?Drive $drive) {
        $device = $drive->device_unix_name;
        $process = Process::fromShellCommandline("sudo ledctl locate=/dev/$device");
        $process->start();
    }
}
