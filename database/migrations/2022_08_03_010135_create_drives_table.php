<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drives', function (Blueprint $table) {
            $table->id();
            $table->string('device_unix_name')->nullable();
            $table->string('device_logical_id')->nullable();
            $table->enum('device_unix_status',
                         [ 'ready', 'converting to 512', 'converting to 520', 'erasing',  'done', 'inactive' ]
            )->nullable();
            $table->decimal('device_conversion_progress')->nullable();
            $table->string('device_serial_number', 255);
            $table->string('device_model_family', 255)->nullable();
            $table->string('device_part_number', 255)->nullable();
            $table->string('device_po_number', 255)->nullable();
            $table->integer('device_blocks')->nullable();
            $table->integer('device_capacity')->nullable();
            $table->integer('device_defects')->nullable();
            $table->integer('device_power_hrs')->nullable();
            $table->string('device_health', 255)->nullable();
            $table->string('erase_method', 255)->nullable();
            $table->string('process_integrity', 255)->nullable();
            $table->string('device_platform_name', 255)->nullable();
            $table->string('device_product_name', 255)->nullable();
            $table->string('device_title', 255)->nullable();
            $table->string('device_type', 255)->nullable();
            $table->string('device_revision', 255)->nullable();
            $table->string('device_size', 255)->nullable();
            $table->timestamp('results_started')->nullable();
            $table->string('results_elapsed', 255)->nullable();
            $table->string('results_errors', 255)->nullable();
            $table->string('result', 255)->nullable();
            $table->string('geometry_bps', 255)->nullable();
            $table->string('geometry_partitioning', 255)->nullable();
            $table->integer('geometry_total_sec')->nullable();
            $table->string('process_name', 255)->nullable();
            $table->timestamp('process_started_at')->nullable();
            $table->string('process_elapsed', 255)->nullable();
            $table->string('process_result', 255)->nullable();
            $table->string('process_conclusion', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->string('fingerprint', 255)->nullable();
            $table->string('drive_type', 255)->nullable();
            $table->string('device_interface', 255)->nullable();
            $table->integer('device_rw')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drives');
    }
};
