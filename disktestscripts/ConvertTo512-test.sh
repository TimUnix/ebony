#!/bin/bash
#diskAttached="false"
#defectiveDisk=""
#message=""
param1=$1
ipaddr=$(hostname -I | awk '{print $1}')

# Check if disk is attached
#while [ "$diskAttached" = "false" ];
#        do
                if [ "$(sudo smartctl --scan | grep $param1 | wc -l)" -lt "1" ]; then
                        echo
                        echo "${yel}Test #1 FAIL: Disk is not detected. Make sure drive is correct.${end}"
                        echo
                        sleep 2
                        exit 0
                fi
                if [ "$(sudo smartctl --scan | grep $param1 | wc -l)" -gt "0" ]; then
                        echo "${mag}INFO: Found disk $1${end}"
                        sleep 2
                        #((diskAttached="true"))
                fi
#        done

#Check if the disk transport protocol is SATA
if [[ $(hdparm -I /dev/$param1 | grep "Transport" | grep "SATA" | wc -l) -eq 0 || $(hdparm -I /dev/$param1 | grep "Transport" | grep "Serial" | wc -l) -eq 0 || $(hdparm -I /dev/$param1 | grep "ATA device" | wc -l) -eq 0 ]]; then
        #statements
        echo "${cyn}SAS disk detected${end}"
        #Check if the disk is 512 block size
        if [ "$(sudo blockdev --getsz /dev/$param1)" -eq 0 ]; then
                echo "${red}Converting drive to 512 block${end}"
                read -p "Press any key to PROCEED convertion"
                sudo sg_format --format --size=512 --fmtpinfo=0 /dev/$param1 -e
                echo "$ipaddr"
                echo "Conversion is now ongoin please monitor progress"
                sleep 3
        else
                echo "${yel}Drive is already in 512 block size${end}"
                echo "$ipaddr"
		sleep 3
        fi
fi
        
