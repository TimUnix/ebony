#!/bin/bash

# # Define colours
# red=$'\e[1;31m'
# grn=$'\e[1;32m'
# yel=$'\e[1;33m'
# blu=$'\e[1;34m'
# mag=$'\e[1;35m'
# cyn=$'\e[1;36m'
# end=$'\e[0m'

# cd /DiskTests/

# if [[ $(sudo lsscsi -L | grep "disk" | egrep -v "SD8SBAT0" | wc -l) -eq 0 ]]; then
# 	while [ true ]; do  
# 		#Clear the screen
# 		clear

# 		#Print banner and version of the script
# 		echo "############################################################################"
# 		echo "${mag}512 to 520 BLOCK SIZE CONVERSION SCRIPT                          VERSION 1.4${end}"
# 		echo "############################################################################"
# 		echo "${yel}INFO: No disk are attached to the server${end}"
# 		echo ""
# 		echo "Probing for disks in three seconds"
# 		sleep 3
# 		if [[ $(sudo lsscsi -L | grep "disk" | egrep -v "SD8SBAT0" | wc -l) -gt 0 ]]; then
# 			break	
# 		fi
# 	done
# fi

# #while [[ true ]]; do
	
# 	# Clear the screen
# 	clear

# 	# Put logical disk names into a file
# 	sudo lsscsi -L | grep "disk" | egrep -v "SD8SBAT0" | awk '{print $NF}' > disksToConvert

# 	#Print banner and version of the script
# 	echo "############################################################################"
# 	echo "${mag}512 TO 520 BLOCK SIZE CONVERSION SCRIPT                          VERSION 1.4${end}"
# 	echo "############################################################################"
# 	echo ""
# 	echo "TOTAL DISKS ATTACHED: "$(cat disksToConvert | wc -l)
# 	echo ""
# 	echo "IS THIS CORRECT Y/N?"

# 	read input
	
# 	if [[ $input == "N" || $input == "n" ]]; then
# 		exit
# 	fi

# #done

#Execute 520 block size conversion with sg_format
# sudo cat disksToConvert | while read drive ; do sudo  sg_format --format --size=520 --fmtpinfo=0 $drive & done 
# exec /bin/bash

for disk in $(sudo lsscsi -L | grep "disk" | egrep -v "SAMSUNG MZ7LH960" | awk '{print $NF}'); do
	sudo sg_format --format --size=520 --fmtpinfo=0 $disk &> /dev/null &
done

while [[ true ]]; do
	# Check how many disks are pending
		/usr/bin/clear
		echo "############################################################################"
		echo "${mag}512 TO 520 BLOCK SIZE CONVERSION SCRIPT                          VERSION 1.4${end}"
		echo "############################################################################"
		echo ""
		echo "TOTAL DISKS PENDING: $(ps aux | grep "sg_format" | grep "sudo" | egrep -v "grep" | wc -l)"
	
    sleep 10
done

# print_banner () {
# 	/usr/bin/clear
# 	echo "############################################################################"
# 	echo "${mag}512 TO 520 BLOCK SIZE CONVERSION SCRIPT                   VERSION 1.5${end}"
# 	echo "############################################################################"
# 	echo ""
# }

# # Get serial number
# sn () {
# 	smartctl -i /dev/$(echo $x |  sed -e 's/drive_converting_//g') | grep "erial" | awk '{print $NF}'
# }

# check_conversion_status () {
# 	for x in drive_converting_*; do 
# 	if [[ $(cat $x | tail -1 | awk '{print $1 " " $2}') == "FORMAT Complete" ]]; then
# 		echo "DISK" "$(echo $x |  sed -e 's/drive_converting_//g')" "| S/N:" $(sn) "| FORMAT Complete"
# 		echo "----------------------------------------------------------------------------"
# 	else
# 		echo "DISK" "$(echo $x |  sed -e 's/drive_converting_//g')" "| S/N:" $(sn) "| $(cat $x | tail -1 | awk '{print $4}')"
# 		echo "----------------------------------------------------------------------------"
# 	fi
# 	done
# }

# convert_drives_520 () {
# 	for disk in $(sudo lsscsi -L | grep "disk" | egrep -v "SD8SBAT0" | awk '{print $NF}'); do
# 		sudo sg_format --format --size=520 --fmtpinfo=0 $disk > drive_converting_$disk & done
# }

# check_drive_attached () {
# 	for x in drive_converting_*; do 
# 		if [[ -b /dev/$(echo $x | sed -e 's/drive_converting_//g') ]]; then
# 			rm $x
# 		fi
# 	done
# }


# # Begin converrion-------------------------------------------------------------------------

# cd /DiskTests
# convert_drives_520

# main_function () {
# while [[ true ]]; do
# 	print_banner
# 	check_conversion_status
# 	check_drive_attached
# 	sleep 3
# done
# }

# # Run the function
# main_function
