#!/bin/bash

#Script by: Richard Ansay
#This is the main programs, it will list the drives according to its slot location.
#This script is written for Ubuntu 20.04 LTS distribution having LSI SAS2X36 RAID Controller (36 Slot)
#it will generate two files that name FrontBayDrives and BackBayDrives, is the listing of drives according to its order
cd /dev/disk/by-path
#pwd
#echo '$~'
if [ -e "FrontBayDrives.txt" ]; then
	rm  FrontBayDrives.txt
fi	
#For the Front Bay
ls -l | grep pci-0000:02:00.0-sas-exp0x50030480209a437f | awk '{print $9,$11}'  | cut  -c44-49,62-66 > /var/www/html/octavia/disktestscripts/DiskTests/FrontBayDrives.txt
if [ -e "BackBayDrives.txt" ]; then
	rm BackBayDrives.txt
fi	
#For the Back Bay
ls -l | grep pci-0000:02:00.0-sas-exp0x50030480183fd4ff | awk '{print $9,$11}'  | cut  -c44-49,62-66 > /var/www/html/octavia/disktestscripts/DiskTests/BackBayDrives.txt
