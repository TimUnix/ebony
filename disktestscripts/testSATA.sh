#! /bin/bash
#Script Updated by: Richard Ansay 
#Parameter passed into the script will be the drive to test
disk=$1;

# Define colors
red=$'\e[1;31m'
grn=$'\e[1;32m'
yel=$'\e[1;33m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'
org=$'\e[1;33m'
bgred=$'\e[1;41m'
bggrn=$'\e[1;42m'
bgyel=$'\e[1;43m'
bgblu=$'\e[1;44m'
bgmag=$'\e[1;45m'
bgcyn=$'\e[1;46m'
bgorg=$'\e[1;43m'
end=$'\e[0m'

# define hdsentinel location
#hdstnl='/var/www/html/octavia/disktestscripts/hdsentinel-019c-x64'

#Print banner and version of the script
echo "${blu}UNIXSURPLUS: SATA DISK TEST SCRIPT                    VERSION 1.6${end}"
echo "################################################################################"
echo "${cyn}TESTING LOGICAL DISK: $disk${end}"

while true
do 
    #Check if the disk is a block device
    if  [ -b /dev/$disk ]; then
        #Check if the disk transport protocol is SATA 
        if [[ $(hdparm -I /dev/$1 | grep "Transport" | grep "SATA" | wc -l) -eq 1 || $(hdparm -I /dev/$1 | grep "Transport" | grep "Serial" | wc -l) -eq 1 || $(hdparm -I /dev/$1 | grep "ATA device" | wc -l) -eq 1 ]]; then
            #statements
            echo "${cyn}SATA disk detected${end}"
            echo ""
	    serial=$(sudo smartctl -T permissive -x /dev/$disk | grep "Serial" | awk '{print $3}')
        else
            while [[ true ]]; do
                clear
                echo "${yel}This is not a SATA disk${end}"
                sleep 3
            done
        fi
	
        cd /var/www/html/DiskTests/
        #file=$(echo /dev/$disk | tr -d "/")
        #echo "smartOutput_$file"
        sudo smartctl -x /dev/$disk > smartOutput_$serial"_"$disk
        cat smartOutput_$serial"_"$disk | grep "odel"
        cat smartOutput_$serial"_"$disk | grep "apacity"
        #cat smartOutput_$file | grep "erial" | grep -v "ATA"
	curr_dir=$(pwd)
	#echo "$cur_dir"
        sudo /var/www/html/ebony/disktestscripts/hdsentinel-019c-x64 -solid | grep "/dev/${disk}" | awk '{print "Serial Number:    " $6}'
	
        # If power on hours are not reported set powerOnHours to 'error'
        if [[ $(cat smartOutput_$serial"_"$disk | grep "Power_On" | awk '{print $NF}' | wc -l) -lt 1 ]]; then
            powerOnHours='error'
        fi
		
        # If power on hours are reported set the powerOnHours variable to that value
        if [[ $(cat smartOutput_$serial"_"$disk | grep "Power_On" | awk '{print $NF}' | wc -l) -gt 0 ]]; then
            if [ $(sudo /var/www/html/ebony/disktestscripts/hdsentinel-019c-x64 -solid | grep "/dev/${disk}" | awk '{print $4}') = "?" ]; then

                powerOnHours=$(cat smartOutput_$serial"_"$disk | grep "Power_On" | awk '{print $NF}')
            else
                powerOnHours=$(sudo /var/www/html/ebony/disktestscripts/hdsentinel-019c-x64 -solid | grep "/dev/${disk}" | awk '{print $4}')
            fi
        fi	
	
	#Grab SMART information from the drives
	if [[ $(cat smartOutput_$serial"_"$disk | grep "ear_Lev" | wc -w) -ge 1 ]]; then
		wear=$(cat smartOutput_$serial"_"$disk | grep "ear_Lev" | awk '{print $NF}')
	else
		wear=0
	fi

	if [[ $(cat smartOutput_$serial"_"$disk | grep "allocated_Even" | wc -w) -ge 1 ]]; then
            reallocatedSectors=$(cat smartOutput_$serial"_"$disk | grep "allocated_Even" | awk '{print $NF}')
        elif [[ $(cat smartOutput_$serial"_"$disk | grep "allocated_Sector" | wc -w) -ge 1 ]]; then
		reallocatedSectors=$(cat smartOutput_$serial"_"$disk | grep "allocated_Sector" | awk '{print $NF}')
        elif [[ $(cat smartOutput_$serial"_"$disk | grep "allocate" | wc -w) -ge 1 ]]; then
	       reallocatedSectors=$(cat smartOutput_$serial"_"$disk | grep "allocate" | awk '{print $NF}')
        fi

        #------------------------------------------------------------
        # Disk grading logic
        # Set Grade to C
        driveGrade="C"

        #Less then 50 Grown Defect, set to Grade B
        if [[ $reallocatedSectors -le 50 ]]; then
            driveGrade="B"
        fi
        #0 Grown Defect, set to Grade A
        if [[ $reallocatedSectors -eq 0 ]]; then 
            driveGrade="A"
        fi

	
        #------------------------------------------------------------
        #Print the SMART information to the user
        #Check for imminent failure
        if [[ $(cat smartOutput_$serial"_"$disk | grep "failure expected" | wc -l) -eq 1 ]]; then
            echo "${red}______________________________________________________${end}"
            echo ""
            echo "${red}WARNING                 : DISK FAILURE IS IMMINENT !!!${end}"
            echo "${red}______________________________________________________${end}"
            echo ""
            defectiveDisk="Fail"
        fi
        # Check SMART health self assessment
        if [[ $(cat smartOutput_$serial"_"$disk | grep "self-assessment" | wc -l) -gt 0 ]]; then
            healthAssesment=$(cat smartOutput_$serial"_"$disk | grep "self-assessment" | awk '{print $NF}')
        else
            healthAssesment="Error getting health assessment"
        fi
	    
        #------------------------------------------------------------
        # Hour value logic
        # < 5 days
        if [[ $powerOnHours -lt 120 ]]; then
            hourValue="${grn}New Drive 0 Hours${end}"
        fi
        # < 30 Days: 
        if [[ $powerOnHours -gt 119 && $powerOnHours -lt 720 ]]; then
            hourValue="${blu}New Pull < 30 Days${end}"
        fi
        
        # < 60 Days:
        if [[ $powerOnHours -gt 719 && $powerOnHours -lt 1440 ]]; then
            hourValue="${org}New Pull < 60 Days${end}"
        fi
        
        # < 3 Years:
        if [[ $powerOnHours -gt 1439 && $powerOnHours -lt 26280 ]]; then
            hourValue="${mag}RFB < 3 Years${end}"
        fi

        # > 3 Years:
        if [[ $powerOnHours -gt 26279 ]]; then
            hourValue="${cyn}USED > 3 Years${end}"
        fi

        # No hours reported - error
        if [[ $powerOnHours == "error" ]]; then
            hourValue="Unable to set HOUR VALUE!"
        fi


        #------------------------------------------------------------
        #Health Information	
        health=`sudo /var/www/html/ebony/disktestscripts/hdsentinel-019c-x64 -solid | grep "${disk}" | awk '{print $3}'`
        echo $health
	if [[ $health -le 50 ]]; then
            if [[ $reallocatedSectors -ge 51 ]]; then 
                driveGrade="c"
            else
                driveGrade="B"
            fi
            if [[ $health -le 25 ]]; then
                driveGrade="C"
            fi
        fi
        #------------------------------------------------------------
		
        echo "################################################################################"
        echo "                            ${mag}DISK TESTING PHASE${end}                              "
        echo "################################################################################"
        #Unmount disk(and its partitions), remove RAID lock and existing paritions, then do a partition test
        echo "${mag}INFO: Unmounting disk and partitions${end}"
        sudo umount -O -f /dev/$disk*
        if [ $? != 0 ]; then
            echo ${mag}Disk is not mounted${end}
        else
            echo ${mag}Disk was unmounted${end}
        fi
	
        #Remove any partitions and or RAID lock
        echo "${mag}INFO: Removing RAID lock and existing partitions${end}"
        sudo wipefs -a /dev/$disk
        if [ $? != 0 ]; then
            echo "${red}ERROR REMOVING RAID LOCK AND OR PARTITIONS${end}"
        else
            echo "${mag}OK${end}"
        fi
        #Create a file system on the disk
        echo "${mag}INFO: Creating a EXT4 file system${end}"
        sudo mkfs.vfat -F 32 -I /dev/$disk
        # sudo mkfs.ext4 -F $disk
        #Check if  creating a file system passed or not (partition test)
        if [ $? != 0 ]; then
            echo "${red}ERROR CREATING A EXT4 FILE SYSTEM${end}"
            defectiveDisk="Fail"
        else
            echo "${mag}OK${end}"
        fi

        # Remove ext4 filesystem
        echo "${mag}INFO: Removing ext4 file system${end}"
        sudo wipefs -a /dev/$disk
        sudo dd if=/dev/zero of=/dev/$disk bs=1M count=100
        if [ $? != 0 ]; then
            echo "${red}ERROR REMOVING EXT4 FILE SYSTEM${end}"
            defectiveDisk="Fail"
        else
            echo "${mag}OK${end}"
        fi
    
        #------------------------------------------------------------
        # Print report
	
        # Change the color of the background depending on the disk grade
        case $driveGrade in
            'A')
                color="$grn"
                bgcolor="$bggrn"
                ;;
            'B')
                color="$cyn"
                bgcolor="$bgcyn"
                ;;
            'C')
                color="$yel"
                bgcolor="$bgyel"
                ;;
        esac
        echo "################################################################################"
        echo "$bgcolor                                                                                "
        echo "                              GRADE $driveGrade LABEL                                   "
        echo "                                                                                "
        echo -e "$end"
        # Print SMART information
        echo "DISK TEST RESULT : PASSED"
        echo "HEALTH ASSESSMENT: $healthAssesment"
        echo "POWER ON HOURS   : $powerOnHours"
        echo "Grown Defects    : $reallocatedSectors"
        echo "GRADE            : ${color}$driveGrade${end}"
        echo "HEALTH           : $health%"
        echo "HOUR VALUE       : $hourValue"
        echo "################################################################################" 
		
        # Re-read partition table
        sudo partprobe /dev/$disk
        echo -n
        read response
        break;
    else
	   # Let the user know that the disk was not detected
	   echo "${yel}Disk was not detected..Checking again in 1 second.${end}"
	   sleep 1
    fi
done
