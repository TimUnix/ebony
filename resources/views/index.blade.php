<x-guest-layout>
    <section class="app-index">
        <article class="app-index__front-bay">
            <header class="app-index__bay-header">Front Bay</header>
            <main>
                <livewire:drive_bay />
            </main>
        </article>
        <article class="app-index__back-bay">
            <header class="app-index__bay-header">Back Bay</header>
            <livewire:drive_bay />
            </article>
    </section>
</x-guest-layout>
