<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        @livewireStyles

        <!-- Scripts -->
        @vite(['resources/sass/app.scss', 'resources/js/app.js'])
        @livewireScripts
    </head>
    <body>
        <header class="app-header">
            <section class="app-header__branding">
                <img src="{{ asset('img/logo.png') }}" class="app-header__brand" alt="App Logo" width="140">
            </section>
            <nav class="app-header__nav">
                <ul class="app-header__routes">
                    <li class="app-header__route"><a class="app-header__link" href="/">Home</a></li>
                </ul>
            </nav>
        </header>
        <main class="app-content">
            {{ $slot }}
        </main>
    </body>
</html>
