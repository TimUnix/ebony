<div class="confirmation-modal">
    <div class="confirmation-modal__modal">
        <div class="confirmation-modal__header">
            <h2 class="confirmation-modal__title">Start Conversion</h2>
        </div>
        <div class="confirmation-modal__body">
            <p class="confirmation-modal__prompt">Are you sure?</p>
            <p class="confirmation-modal__message">{{ $message }}</p>
        </div>
        <div class="confirmation-modal__footer">
            <button type="button" class="confirmation-modal__cancel" wire:click.stop="$emitUp('conversionCancelled')">
                Cancel
            </button>
            <button type="button" class="confirmation-modal__confirm" wire:click.stop="confirmConversion">
                Confirm
            </button>
        </div>
    </div>
</div>
