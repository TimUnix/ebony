<div class="device"
    wire:poll.60s="refreshProgress('{{ $drive->device_unix_name }}', '{{ $drive->device_serial_number }}')">
    <div class="device__grade">
        @if (!isset($drive->device_defects))
            <div class="device__grade-none"><i class="fa-solid fa-exclamation"></i></div>
        @elseif ($drive->device_defects < 3)
            <div class="device__grade-a"><p>A</p></div>
        @elseif ($drive->device_defects > 2 && $drive->device_defects <= 50)
            <div class="device__grade-b"><p>B</p></div>
        @else
            <div class="device__grade-c"><p>C</p></div>
        @endif
    </div>
    <div class="device__info">
        <p>Status: <em class="device__status">{{ ucfirst($drive->device_unix_status) }}</em></p>
        <p>
            @if ($drive->device_capacity >= 1000)
            {{ $drive->device_capacity / 1000 }} TB
            @else
            {{ $drive->device_capacity }} GB
            @endif
            - {{ $drive->device_blocks }} blocks
        </p>
        <p>Serial: {{ $drive->device_serial_number }}</p>
        <p>Part Number: {{ $drive->device_part_number }}</p>
    </div>
    <div class="device__actions">
        @if (str($drive->device_unix_status)->contains('converting'))
        <p class="device__center">
            Progress: {{ $drive->device_conversion_progress }}%
        </p>
        @else
            @if ($drive->device_blocks === 512)
                <button class="device__convert device__button" type="button" wire:click.stop="handleConvertClicked">
                    <i class="fa-solid fa-hard-drive"></i>
                    Convert to 520
                </button>
            @else
                <button class="device__convert device__button" type="button" wire:click.stop="handleConvertClicked">
                    <i class="fa-solid fa-hard-drive"></i>
                    Convert to 512
            @endif
        @endif
        <button type="button" class="device__locate device__button" wire:click.stop="handleLocateClicked">
            <i class="fa-solid fa-location-arrow"></i>
            Locate
        </button>
    </div>
</div>
