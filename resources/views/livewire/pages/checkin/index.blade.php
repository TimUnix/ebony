<div class="checkin">
    <h1>Check In Device</h1>
    <form class="checkin__form" wire:submit.prevent="save">
        <label>
            PO Number
            <input type="text" wire:model="poNumber" required>
            @error('poNumber') <span>{{ $message }}</span> @enderror
        </label>
        <label>
            Part Number
            <input type="text" wire:model="partNumber" required>
            @error('partNumber') <span>{{ $message }}</span> @enderror
        </label>
        <label>
            Capacity
            <input type="number" wire:model="capacity" required>
            @error('capacity') <span>{{ $message }}</span> @enderror
            <select wire:model="measurement" required>
                <option value="0">GB</option>
                <option value="1">TB</option>
            </select>
        </label>
        <label>
            Serial Number
            <input type="text" wire:model="serialNumber" required>
            @error('serialNumber') <span>{{ $message }}</span> @enderror
        </label>

        <div class="checkin__actions">
            <button class="checkin__button checkin__form-clear" type="reset">Clear</button>
            <button class="checkin__button checkin__form-submit" type="submit">Check In</button>
        </div>
    </form>
</div>
