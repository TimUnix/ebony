<div class="home" wire:poll="refreshDrives">
    <div>
        @if ($modalShown)
        <livewire:confirmation-modal />
        @endif
    </div>

    <div class="home__header">
        <h1 class="home__heading">Drives</h1>
    </div>
    <div class="home__summary">
        <div class="home__blocks">
            <p class="home__drive-count">Total Drives: {{ $this->driveCount }}</p>
            <div class="home__block">
                <button class="home__button">
                    <i class="fa-solid fa-eraser"></i>
                    <span class="home__button-label">
                        Erase 3 Pass
                    </span>
                </button>
                <p>512 Blocks: {{ $this->status['512'] }}</p>
                <button class="home__button" wire:click.stop="promptMultipleConversion(520)">
                    <i class="fa-solid fa-hard-drive"></i>
                    <span class="home__button-label">
                        Convert to 520
                    </span>
                </button>
            </div>
            <div class="home__block">
                <button class="home__button">
                    <i class="fa-solid fa-eraser"></i>
                    <span class="home__button-label">
                        Erase 3 Pass
                    </span>
                </button>
                <p>520 Blocks: {{ $this->status['other'] }}</p>
                <button class="home__button" wire:click.stop="promptMultipleConversion(512)">
                    <i class="fa-solid fa-hard-drive"></i>
                    <span class="home__button-label">
                        Convert to 512
                    </span>
                </button>
            </div>
        </div>
        <p class="home__status home__chip">Converting to 512: {{ $this->status['convertingTo512'] }}</p>
        <p class="home__status home__chip">Converting to 520: {{ $this->status['convertingTo520'] }}</p>
        <div class="home__overview">
            @foreach ($this->summary as $capacity => $count)
            <p class="home__chip">
                @if (intval($capacity) >= 1000)
                {{ $capacity / 1000 }} TB:
                @else
                {{ $capacity }} GB:
                @endif
                {{ $count }} @if ($count > 1) drives @else drive @endif
            </p>
            @endforeach
        </div>
    </div>
    <div class="home__devices">
        @if ($drives?->count() > 0)
        @foreach ($drives as $drive)
        <livewire:device :drive="$drive" :wire:key="$drive->device_serial_number" />
        @endforeach
        @else
        <div class="home__empty-info">
            <p>No drives have been detected</p>
        </div>
        @endif
    </div>
</div>
