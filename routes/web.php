<?php

use Illuminate\Support\Facades\Route;

use App\Http\Livewire\Pages\Home as HomePage;
use App\Http\Livewire\Pages\Checkin\Checkin as CheckinPage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', HomePage::class)->name('home');
Route::get('/drives/new', CheckinPage::class)->name('checkin');
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
